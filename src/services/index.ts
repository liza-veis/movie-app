import { Http } from './http/http.service';
import { Storage } from './storage/storage.service';
import { Movie } from './movie/movie.service';

const storage = new Storage();

const http = new Http();

const movie = new Movie({
    http,
    storage,
});

export { movie as MovieService };
