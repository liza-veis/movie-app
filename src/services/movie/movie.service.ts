import { API_REQUEST_URL, STORAGE_KEY } from '../../common/constants';
import { getMovieFromResponse } from '../../helpers';
import { MovieData, MovieResponse, MoviesResponse } from '../../types';
import { Http } from '../http/http.service';
import { Storage } from '../storage/storage.service';

type MovieServiceConfig = {
    http: Http;
    storage: Storage;
};

export class Movie {
    private http: Http;

    private storage: Storage;

    constructor({ http, storage }: MovieServiceConfig) {
        this.http = http;
        this.storage = storage;
    }

    async search(query: string, page = 1): Promise<MovieData[]> {
        const { results } = await this.http.load<MoviesResponse>(
            API_REQUEST_URL.SEARCH,
            { query: { query, page } }
        );

        return results.map(getMovieFromResponse);
    }

    async getPopular(page = 1): Promise<MovieData[]> {
        const { results } = await this.http.load<MoviesResponse>(
            API_REQUEST_URL.GET_POPULAR,
            { query: { page } }
        );

        return results.map(getMovieFromResponse);
    }

    async getTopRated(page = 1): Promise<MovieData[]> {
        const { results } = await this.http.load<MoviesResponse>(
            API_REQUEST_URL.GET_TOP_RATED,
            { query: { page } }
        );

        return results.map(getMovieFromResponse);
    }

    async getUpcoming(page = 1): Promise<MovieData[]> {
        const { results } = await this.http.load<MoviesResponse>(
            API_REQUEST_URL.GET_UPCOMING,
            { query: { page } }
        );

        return results.map(getMovieFromResponse);
    }

    async getDetails(id: number): Promise<MovieData> {
        const movie = await this.http.load<MovieResponse>(
            `${API_REQUEST_URL.GET_DETAILS}/${id}`
        );

        return getMovieFromResponse(movie);
    }

    addToFavorite(id: number): boolean {
        const moviesId = this.getFavoriteMoviesId();

        if (moviesId.includes(id)) {
            return false;
        }

        this.setFavoriteMoviesId([...moviesId, id]);
        return true;
    }

    removeFromFavorite(id: number): boolean {
        const moviesId = this.getFavoriteMoviesId();
        const movieIndex = moviesId.indexOf(id);

        if (movieIndex === -1) {
            return false;
        }

        moviesId.splice(movieIndex, 1);
        this.setFavoriteMoviesId(moviesId);

        return true;
    }

    getFavoriteMoviesId = (): number[] => {
        return JSON.parse(
            this.storage.getItem(STORAGE_KEY.FAVORITE_FILMS) || '[]'
        );
    };

    private setFavoriteMoviesId(moviesId: number[]) {
        this.storage.setItem(
            STORAGE_KEY.FAVORITE_FILMS,
            JSON.stringify(moviesId)
        );
    }
}
