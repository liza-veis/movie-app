import { MOVIE_DB_API_KEY } from '../../common/config';

type HttpOptions = {
    method?: string;
    payload?: BodyInit;
    query?: Query;
};

type Query = {
    [property: string]: string | number;
};

export class Http {
    load<T>(url: string, options: HttpOptions = {}): Promise<T> {
        const { method = 'GET', payload = null, query = {} } = options;

        return fetch(this.getUrl(url, this.addApiKey(query)), {
            method,
            body: payload,
        })
            .then(this.checkStatus)
            .then(this.parseJSON)
            .catch(this.throwError);
    }

    private async checkStatus(response: Response) {
        if (!response.ok) {
            const parsedException = await response.json();

            throw new Error(parsedException?.message ?? response.statusText);
        }

        return response;
    }

    private getUrl(url: string, query: Query) {
        const queryString = Object.entries(query)
            .map(([property, value]) => `${property}=${value}`)
            .join('&');

        return `${url}?${queryString}`;
    }

    private addApiKey(query: Query) {
        return { ...query, api_key: MOVIE_DB_API_KEY || '' };
    }

    private parseJSON(response: Response) {
        return response.json();
    }

    private throwError(err: Error) {
        throw err;
    }
}
