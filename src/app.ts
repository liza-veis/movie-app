import { Movie as MovieService } from './services/movie/movie.service';
import { Movie } from './components/movie';
import { Category, MovieData } from './types';
import { DATA_ATTRIBUTE } from './common/constants';

type AppProps = {
    movieService: MovieService;
    movieRoot?: HTMLElement;
    favoriteMoviesRoot?: HTMLElement;
    category?: Category;
};

export class App {
    private movieRoot: HTMLElement;

    private favoriteMoviesRoot: HTMLElement;

    private movieService: MovieService;

    private category: Category = 'popular';

    private favoriteMovies: Map<number, HTMLElement[]> = new Map();

    private page = 1;

    private query = '';

    constructor({
        movieRoot = document.body,
        favoriteMoviesRoot = document.body,
        category = 'popular',
        movieService,
    }: AppProps) {
        this.movieService = movieService;
        this.movieRoot = movieRoot;
        this.favoriteMoviesRoot = favoriteMoviesRoot;
        this.category = category;

        this.movieService.getFavoriteMoviesId().forEach((id) => {
            this.favoriteMovies.set(id, []);
        });
        this.loadMovies();
        this.loadFavoriteMovies();

        this.toggleIsFavorite = this.toggleIsFavorite.bind(this);
    }

    async loadMovies(): Promise<void> {
        let movies: MovieData[];

        if (this.query) {
            movies = await this.movieService.search(this.query, this.page);
        } else {
            movies = await this.getCategoryMovies();
        }

        const elements = movies.map((movie) => {
            const movieElements = this.favoriteMovies.get(movie.id);
            const element = Movie({
                ...movie,
                handleIsFavoriteSet: this.toggleIsFavorite,
            });

            if (movieElements) {
                movieElements[0] = element;
                element.setAttribute(DATA_ATTRIBUTE.IS_FAVORITE, 'true');
            }

            return element;
        });

        this.movieRoot.append(...elements);
    }

    async loadFavoriteMovies(): Promise<void> {
        this.resetFavouriteMovies();

        const moviesId = await Promise.all(
            Array.from(this.favoriteMovies.keys()).map((id) =>
                this.movieService.getDetails(id)
            )
        );

        const elements = moviesId.map((movie) => {
            const movieElements = this.favoriteMovies.get(movie.id);
            const element = Movie({
                ...movie,
                isFullWidth: true,
                handleIsFavoriteSet: this.toggleIsFavorite,
            });

            if (movieElements) {
                movieElements[1] = element;
                element.setAttribute(DATA_ATTRIBUTE.IS_FAVORITE, 'true');
            }

            return element;
        });

        this.favoriteMoviesRoot.append(...elements);
    }

    resetMovies(): void {
        this.movieRoot.innerHTML = '';
    }

    resetFavouriteMovies(): void {
        this.favoriteMoviesRoot.innerHTML = '';
    }

    setCategory(category: Category): void {
        if (category === this.category) {
            return;
        }

        this.page = 1;
        this.query = '';
        this.category = category;
        this.resetMovies();
        this.loadMovies();
    }

    search(query: string): void {
        if (query === this.query) {
            return;
        }

        this.page = 1;
        this.query = query;
        this.resetMovies();
        this.loadMovies();
    }

    loadMore(): void {
        this.page += 1;
        this.loadMovies();
    }

    async getRandomMovie(): Promise<MovieData> {
        const movies = await this.movieService.getPopular(this.page);
        const randomIndex = Math.floor(Math.random() * movies.length);

        return movies[randomIndex];
    }

    private async getCategoryMovies() {
        let movies: MovieData[];

        switch (this.category) {
            case 'upcoming':
                movies = await this.movieService.getUpcoming(this.page);
                break;

            case 'top_rated':
                movies = await this.movieService.getTopRated(this.page);
                break;

            default:
                movies = await this.movieService.getPopular(this.page);
                break;
        }

        return movies;
    }

    private toggleIsFavorite(id: number, movieElement: HTMLElement) {
        const movieElements = this.favoriteMovies.get(id);
        let success: boolean;

        if (movieElements) {
            success = this.movieService.removeFromFavorite(id);

            if (success) {
                movieElements[0]?.setAttribute(
                    DATA_ATTRIBUTE.IS_FAVORITE,
                    'false'
                );
                movieElements[1]?.remove();
                this.favoriteMovies.delete(id);
            }
        } else {
            success = this.movieService.addToFavorite(id);

            if (success) {
                movieElement.setAttribute(DATA_ATTRIBUTE.IS_FAVORITE, 'true');
                this.favoriteMovies.set(id, [movieElement]);
                this.loadFavoriteMovies();
            }
        }
    }
}
