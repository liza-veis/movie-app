const API_BASE_URL = 'https://api.themoviedb.org/3';

export const IMAGE_BASE_URL = 'https://image.tmdb.org/t/p/w500';

export const API_REQUEST_URL = {
    SEARCH: `${API_BASE_URL}/search/movie`,
    GET_POPULAR: `${API_BASE_URL}/movie/popular`,
    GET_TOP_RATED: `${API_BASE_URL}/movie/top_rated`,
    GET_UPCOMING: `${API_BASE_URL}/movie/upcoming`,
    GET_DETAILS: `${API_BASE_URL}/movie`,
};

export const STORAGE_KEY = {
    FAVORITE_FILMS: 'favorite_films',
};

export const DATA_ATTRIBUTE = {
    IS_FAVORITE: 'data-is-favorite',
};
