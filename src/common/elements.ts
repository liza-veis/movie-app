export const movieRootElement = document.getElementById('film-container');
export const favoriteMoviesRootElement =
    document.getElementById('favorite-movies');
export const categoryElement = document.getElementById('button-wrapper');
export const loadMoreElement = document.getElementById('load-more');
export const submitElement = document.getElementById('submit');
export const searchElement =
    document.querySelector<HTMLInputElement>('#search');
export const randomMovie = document.getElementById('random-movie');
export const randomMovieName = document.getElementById('random-movie-name');
export const randomMovieDescription = document.getElementById(
    'random-movie-description'
);
