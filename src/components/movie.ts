import { createElement, createSvg } from '../helpers';
import { MovieData } from '../types';

type MovieProps = MovieData & {
    handleIsFavoriteSet: (id: number, movieElement: HTMLElement) => void;
    isFullWidth?: boolean;
};

const heartPath = `
    <path
        fill-rule="evenodd"
        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
    />
`;

export const Movie = ({
    id,
    image: imageUrl,
    overview: overviewContent,
    date: dateContent,
    handleIsFavoriteSet,
    isFullWidth = false,
}: MovieProps): HTMLElement => {
    const movie = createElement({
        className: `${!isFullWidth ? 'col-lg-3 col-md-4 ' : ''}col-12 p-2`,
    });
    const cardShadow = createElement({
        className: 'card shadow-sm',
    });
    const image = createElement({
        tagName: 'img',
        attributes: {
            src: imageUrl,
        },
    });
    const heart = createSvg({
        className: 'bi bi-heart-fill position-absolute p-2 heart',
        innerHTML: heartPath,
        attributes: {
            stroke: 'red',
            width: '50',
            height: '50',
            viewBox: '0 -2 18 22',
        },
    });
    const cardBody = createElement({
        className: 'card-body',
    });
    const overview = createElement({
        tagName: 'p',
        className: 'card-text truncate',
        textContent: overviewContent,
    });
    const dateContainer = createElement({
        className: 'd-flex justify-content-between align-items-center',
    });
    const date = createElement({
        tagName: 'small',
        className: 'text-muted',
        textContent: dateContent || 'already released',
    });

    heart.addEventListener('click', () => {
        handleIsFavoriteSet(id, movie);
    });

    dateContainer.append(date);
    cardBody.append(overview, dateContainer);
    cardShadow.append(image, heart, cardBody);
    movie.append(cardShadow);

    return movie;
};
