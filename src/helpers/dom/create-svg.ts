type createSvgOptions = {
    className?: string;
    innerHTML?: string;
    attributes?: {
        [name: string]: string | boolean;
    };
};

export const createSvg = ({
    className,
    innerHTML,
    attributes = {},
}: createSvgOptions = {}): SVGSVGElement => {
    const element = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'svg'
    );

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    if (innerHTML) {
        element.innerHTML = innerHTML;
    }

    Object.entries(attributes).forEach(([name, value]) => {
        if (typeof value === 'boolean') {
            if (Boolean(element.getAttribute(name)) !== value) {
                element.toggleAttribute(name);
            }
        } else {
            element.setAttribute(name, value);
        }
    });

    return element;
};
