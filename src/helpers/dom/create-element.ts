type createElementOptions = {
    tagName?: string;
    className?: string;
    textContent?: string;
    attributes?: {
        [name: string]: string | boolean;
    };
};

export const createElement = ({
    tagName = 'div',
    className,
    textContent,
    attributes = {},
}: createElementOptions = {}): HTMLElement => {
    const element = document.createElement(tagName);

    if (className) {
        const classNames = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }
    if (textContent) {
        element.textContent = textContent;
    }

    Object.entries(attributes).forEach(([name, value]) => {
        if (typeof value === 'boolean') {
            if (Boolean(element.getAttribute(name)) !== value) {
                element.toggleAttribute(name);
            }
        } else {
            element.setAttribute(name, value);
        }
    });

    return element;
};
