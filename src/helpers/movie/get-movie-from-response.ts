import { IMAGE_BASE_URL } from '../../common/constants';
import { MovieData, MovieResponse } from '../../types';

export const getMovieFromResponse = (response: MovieResponse): MovieData => {
    const {
        id,
        title,
        overview,
        poster_path,
        backdrop_path,
        release_date: date,
    } = response;

    return {
        id,
        title,
        overview,
        image: `${IMAGE_BASE_URL}/${poster_path}`,
        backdrop: `${IMAGE_BASE_URL}/${backdrop_path}`,
        date,
    };
};
