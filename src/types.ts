export type MovieResponse = {
    id: number;
    title: string;
    overview: string;
    poster_path: string;
    backdrop_path: string;
    release_date: string;
};

export type MoviesResponse = {
    results: MovieResponse[];
};

export type MovieData = {
    id: number;
    title: string;
    overview: string;
    date: string;
    image: string;
    backdrop: string;
};

export type Category = 'popular' | 'upcoming' | 'top_rated';

export type CategoryInput = HTMLInputElement & { id: Category };
