import { MovieService } from './services';
import {
    movieRootElement,
    favoriteMoviesRootElement,
    categoryElement,
    loadMoreElement,
    submitElement,
    searchElement,
    randomMovie,
    randomMovieName,
    randomMovieDescription,
} from './common/elements';
import { App } from './app';
import { CategoryInput } from './types';

export const render = async (): Promise<void> => {
    if (!movieRootElement || !favoriteMoviesRootElement) {
        return;
    }

    const startCategory =
        categoryElement?.querySelector<CategoryInput>(':checked')?.id;

    const app = new App({
        movieRoot: movieRootElement,
        favoriteMoviesRoot: favoriteMoviesRootElement,
        movieService: MovieService,
        category: startCategory,
    });

    app.getRandomMovie().then(({ backdrop, title, overview }) => {
        if (randomMovie) {
            randomMovie.style.backgroundImage = `url(${backdrop})`;
        }
        if (randomMovieName) {
            randomMovieName.textContent = title;
        }
        if (randomMovieDescription) {
            randomMovieDescription.textContent = overview;
        }
    });

    categoryElement?.addEventListener('change', (event) => {
        const category = (<CategoryInput>event.target).id;
        app.setCategory(category);

        if (searchElement) {
            searchElement.value = '';
        }
    });

    loadMoreElement?.addEventListener('click', () => {
        app.loadMore();
    });

    submitElement?.addEventListener('click', () => {
        app.search(searchElement?.value || '');
    });
};
